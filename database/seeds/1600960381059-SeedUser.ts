import { MigrationInterface, QueryRunner } from 'typeorm';
import { TableName } from 'src/utils';

export class SeedUser1600960381059 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO users (first_name, last_name, is_admin, email, password, password_salt) values 
        ("Tom", "Mery", true, "tubr1@gmail.com", "$2b$10$7.sW1/a/3OvdbEH5k4GGwOasPcb5HNT1kNnlU4ViPaXG5VIzf12HO", "$2b$10$7.sW1/a/3OvdbEH5k4GGwO"),
        ("Nguyễn", "Thành", true, "admin@gmail.com", "$2b$10$7.sW1/a/3OvdbEH5k4GGwOasPcb5HNT1kNnlU4ViPaXG5VIzf12HO", "$2b$10$7.sW1/a/3OvdbEH5k4GGwO"),
        ("Join", "Smith", DEFAULT, "join.smith@gmail.com", "$2b$10$7.sW1/a/3OvdbEH5k4GGwOasPcb5HNT1kNnlU4ViPaXG5VIzf12HO", "$2b$10$7.sW1/a/3OvdbEH5k4GGwO"),
        ("Mark", "Taylor", DEFAULT, "taylor123@gmail.com", "$2b$10$7.sW1/a/3OvdbEH5k4GGwOasPcb5HNT1kNnlU4ViPaXG5VIzf12HO", "$2b$10$7.sW1/a/3OvdbEH5k4GGwO")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(TableName.User, true);
  }
}
