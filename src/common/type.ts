export interface PaginationInput {
  limit?: number;
  page?: number;
  search?: string;
}
