/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UsePipes,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { State } from 'src/decorators/state.decorator';
import { JoiValidationPipe } from 'src/pipes/joi-validation.pipe';
import { TrimPipe } from 'src/pipes/trim.pipe';
import { DeepPartial } from 'typeorm';
import UserEntity from '../entity/user.entity';
import { ToggleActivateObject } from '../interfaces/deactive.interface';
import { NewUserInput } from '../interfaces/new-user.interface';
import { UpdateUserInput } from '../interfaces/update-user.interface';
import { UserPaginationResponse } from '../interfaces/user-pagination.interface';
import { UserPaginationQuery } from '../interfaces/user.interface';
import { createUserSchema } from '../schemas/create-user.schema';
import { deactiveSchema } from '../schemas/deactive.schema';
import { updateUserSchema } from '../schemas/update-user.schema';
import { UserService } from '../services/user.service';

@ApiTags('users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({
    summary: 'Get some users',
    description: 'Get some users, support paging',
  })
  @Get()
  @ApiResponse({
    status: HttpStatus.OK,
    description: '',
    type: UserPaginationResponse,
  })
  async getAll(@Query() query: UserPaginationQuery) {
    return await this.userService.getAllPaging(query);
  }

  @ApiOperation({
    summary: 'Create an user',
    description: 'Create an user',
  })
  @Post()
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
    type: UserEntity,
  })
  @UsePipes(new JoiValidationPipe(createUserSchema))
  async createUser(@Body(TrimPipe) body: NewUserInput) {
    const user = this.userService.getByEmail(body.email);

    if (user) {
      throw new HttpException('Email existed', HttpStatus.BAD_REQUEST);
    }

    return await this.userService.create(body);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Get user information',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: UserEntity,
  })
  getById(
    @State('user') user: UserEntity,
    @Param('id') id: string,
  ): UserEntity {
    return user;
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete an user',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Success',
    type: Boolean,
  })
  async deleteById(@State('user') user: UserEntity, @Param('id') id: string) {
    return !!(await this.userService.deleteById(user.id));
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'Update an user',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: UserEntity,
  })
  @ApiBody({
    type: UpdateUserInput,
  })
  @UsePipes(new JoiValidationPipe(updateUserSchema))
  async updateUser(
    @State('user') user: UserEntity,
    @Body(TrimPipe) body: DeepPartial<UserEntity>,
    @Param('id') id: string,
  ) {
    return await this.userService.update(body, user);
  }

  @Put(':id')
  @ApiOperation({
    summary: 'Update an user',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: UserEntity,
  })
  @ApiBody({
    type: UpdateUserInput,
  })
  @UsePipes(new JoiValidationPipe(updateUserSchema))
  async updateUserByPutRequest(
    @State('user') user: UserEntity,
    @Body(TrimPipe) body: DeepPartial<UserEntity>,
    @Param('id') id: string,
  ) {
    return await this.userService.update(body, user);
  }

  @Post(':id/activate')
  @ApiOperation({
    summary: 'Active/Deactive an user',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: UserEntity,
  })
  @UsePipes(new JoiValidationPipe(deactiveSchema))
  async deactiveUser(
    @State('user') user: UserEntity,
    @Body() body: ToggleActivateObject,
    @Param('id') id: string,
  ) {
    return await this.userService.update({ isActive: body.isActive }, user);
  }
}
