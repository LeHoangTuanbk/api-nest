import * as Joi from 'joi';
import { ToggleActivateObject } from '../interfaces/deactive.interface';

export const deactiveSchema = Joi.object<ToggleActivateObject>({
  isActive: Joi.bool().required(),
});
