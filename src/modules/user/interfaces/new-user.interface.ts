import { ApiProperty } from '@nestjs/swagger';

export class NewUserInput {
  @ApiProperty({
    title: 'First Name',
    required: true,
  })
  firstName: string;

  @ApiProperty({
    title: 'Last Name',
    required: true,
  })
  lastName: string;

  @ApiProperty({
    title: 'Is admin',
  })
  isAdmin?: boolean;

  @ApiProperty({
    title: 'Is active',
  })
  isActive?: boolean;

  @ApiProperty({
    title: 'Deactive reason',
  })
  reason?: string;

  @ApiProperty({
    title: 'Email',
    required: true,
    format: 'email',
  })
  email: string;

  @ApiProperty({
    title: 'Password',
    required: true,
  })
  password: string;
}
